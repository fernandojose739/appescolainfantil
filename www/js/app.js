var app = angular.module('app', [
    'ionic',
    'ngCordova'
]);

app.run(function ($ionicPlatform, localStorageService) {
    $ionicPlatform.ready(function () {

        var push = PushNotification.init({
            android: {
                senderID: "126606885082",
                icon: "icon"
            },
            ios: {
                alert: "true",
                badge: "true",
                sound: "true"
            },
            windows: {}
        });

        push.on('registration', function (data) {
            localStorageService.salvar('pushNotificationAndroidRegistrationId', data.registrationId);
        });

        push.on('notification', function (data) {
            // data.message,
            // data.title,
            // data.count,
            // data.sound,
            // data.image,
            // data.additionalData
        });

        push.on('error', function (e) {
            alert(e.message);
        });

        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }

        if (window.StatusBar) {
            window.StatusBar.overlaysWebView(false);
            window.StatusBar.styleHex('#FFFFFF');
        }

        window.addEventListener('native.keyboardshow', function () {
            document.body.classList.add('keyboard-open');
        });
    });
});