(function () {
	'use strict';

	app.service('usuarioService', usuarioService);

	usuarioService.$inject = ['baseService',
		'$q',
		'escolaService',
		'localStorageService'];

	function usuarioService(baseService, $q, escolaService, localStorageService) {

		var service = this;

		service.login = login;
		service.listar = listar;
		service.atualizaPushNotificationAndroidRegistrationId = atualizaPushNotificationAndroidRegistrationId;
		service.obterDoLocalStorage = obterDoLocalStorage;
		service.obterResgistraionIdPorUsuario = obterResgistraionIdPorUsuario;

		return service;

		function obterDoLocalStorage(key) {
			if (localStorageService.obter(key) == undefined || localStorageService.obter(key) == null) {
				return;
			};

			var valor = JSON.parse(localStorageService.obter(key));
			return valor;
		};

		function atualizaPushNotificationAndroidRegistrationId() {
			var pushNotificationAndroidRegistrationId = obterDoLocalStorage('pushNotificationAndroidRegistrationId');
			var usuario = obterDoLocalStorage('usuario');

			if (usuario == null || usuario == undefined) return;
			//if (pushNotificationAndroidRegistrationId === usuario.PushNotificationAndroidRegistrationId) return;

			usuario.PushNotificationAndroidRegistrationId = pushNotificationAndroidRegistrationId;
			localStorageService.salvar("usuario", usuario);

			var request = {
				Id: usuario.UsuarioId,
				PushNotificationAndroidRegistrationId: pushNotificationAndroidRegistrationId
			};

			var def = $q.defer();
			baseService.call("POST", "usuario/atualiza-PushNotificationAndroidRegistrationId", request)
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		}

		function login(email, senha) {
			var def = $q.defer();
			baseService.call("GET", "usuario/login/" + email + "/" + senha, "")
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		};

		function listar() {
			var escola = escolaService.obterEscola();
			var def = $q.defer();
			baseService.call("GET", "usuario/listar/" + escola.Id, "")
				.then(function (data) {
					localStorageService.salvar('usuarios', data.data);
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		};

		function obterResgistraionIdPorUsuario(usuarioId) {
			var usuarios = JSON.parse(localStorageService.obter('usuarios'));
			for (var x = 0; usuarios.length; x++) {
				if (usuarioId == usuarios[x].Id) {
					return usuarios[x].PushNotificationAndroidRegistrationId;
				}
			}
		}
	}
})();