(function () {
	'use strict';

	app.service('alunoService', alunoService);

	alunoService.$inject = ['baseService', '$q', 'escolaService', 'usuarioService'];

	function alunoService(baseService, $q, escolaService, usuarioService) {

		var service = this;

		service.listar = listar;

		return service;

		function listar(usuarioId) {
			var usuario = usuarioService.obterDoLocalStorage();
			var escola = escolaService.obterEscola();

			var def = $q.defer();
			baseService.call("GET", "aluno/" + usuario.Id + "/" + escola.Id, "")
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		};
	}
})();