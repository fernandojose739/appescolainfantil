(function () {
	'use strict';

	app.service('medicamentoService', medicamentoService);

	medicamentoService.$inject = ['baseService', '$q', 'escolaService'];

	function medicamentoService(baseService, $q, escolaService, usuarioService) {

		var service = this;

		service.listar = listar;
		service.salvar = salvar;
		service.remover = remover;

		return service;

		function listar() {
			var usuario = usuarioService.obterDoLocalStorage();
			var escola = escolaService.obterEscola();
			var def = $q.defer();
			baseService.call("GET", "medicamento/" + usuario.Id + "/" + escola.Id, "")
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		}

		function salvar(medicamento){
			var usuario = usuarioService.obterDoLocalStorage();
			var escola = escolaService.obterEscola();
			var def = $q.defer();
			medicamento.EscolaId = escola.Id;
			medicamento.UsuarioNome = usuario.Nome;
			baseService.call("POST", "medicamento/salvar", medicamento)
				.then(function(data){
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		}

		function remover() {
            var def = $q.defer();
            baseService.call("POST", "medicamento/remover", request)
                .then(function (data) {
                    def.resolve(data);
                }, function (error) {
                    def.reject(error);
                });
            return def.promise;
        };
	}
})();