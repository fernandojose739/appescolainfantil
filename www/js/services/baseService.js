﻿(function () {
	'use strict';

	app.factory('baseService', BaseService);

	BaseService.$inject = ['configApp', '$http'];

	function BaseService(configApp, $http) {

		var dataFactory = {};

		dataFactory.call = function (method, url, request) {
			var req = {
				method: method,
				url: configApp.serverUrl + url,
				headers: {
					'Content-Type': 'application/json',
					'Token': '5b5fd8ba4bb6d9e12bf93d5373409ee7'
				},
				data: JSON.stringify(request)
			}

			return $http(req);
		};

		dataFactory.enviarNotificacao = function (request) {
			var req = {
				method: 'POST',
				url: 'https://gcm-http.googleapis.com/gcm/send',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'key=AIzaSyCIRsc_g0fMh0NxdB9hVizoDJldsMw3-hs'
				},
				data: JSON.stringify(request)
			}

			return $http(req);
		};

		return dataFactory;
	}
})();