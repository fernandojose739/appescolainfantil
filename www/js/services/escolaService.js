(function () {
	'use strict';

	app.service('escolaService', escolaService);

	escolaService.$inject = ['localStorageService', 
							 '$location'];

	function escolaService(localStorageService, $location) {

		var service = this;

		service.obterEscola = obterEscola;

		return service;

		function obterEscola() {
			var usuario = JSON.parse(localStorageService.obter('usuario'));
			if(usuario == undefined || usuario == null){
				$location.path('/login');
			}

			return usuario.Escola;
		}
	}
})();