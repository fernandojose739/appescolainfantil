(function () {
	'use strict';

	app.service('notificacaoService', notificacaoService);

	notificacaoService.$inject = ['baseService', '$q'];

	function notificacaoService(baseService, $q) {

		var service = this;

		service.enviar = enviar;

		return service;

		function enviar(request){
			baseService.enviarNotificacao(request);
		};
	}
})();