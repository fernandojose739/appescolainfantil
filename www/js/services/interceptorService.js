﻿(function () {
	'use strict';

	app.factory('errorService', ErrorService);

	ErrorService.$inject = ['$q', '$location'];

	function ErrorService($q, $location) {

		var service = this;

		service.ListarError = listarError;

		return service;

		function listarError(params) {

			var def = $q.defer();

			if (params.status == 204) {
				def.resolve("Nenhum veículo encontrado");
			} else {
				def.reject("Error: ");
			}

			return def.promise;
		} 
            
		// return {

		//     responseError: function (rejection) {
		//         console.log('Failed with', rejection.status, 'status');                
                
		//         if (rejection.status == 204) {
		//             console.log("victor");  
		//         }
                
		//         if (rejection.status == 403) {
		//             console.log('Acesso negado');
		//             //$location.url('/Error/403');
		//         }

		//         if (rejection.status == 404) {
		//             console.log('Página não encontrada');
		//             //$location.url('/Error/404');
		//         }

		//         if (rejection.status == 500) {
		//             console.log('Erro no servidor');
		//             //$location.url('/Error/500');
		//         }

		//         return $q.reject(rejection);
		//     }
		// }
		
	}
})();
