(function () {
	'use strict';

	app.service('atividadeService', atividadeService);

	atividadeService.$inject = ['baseService', '$q', 'escolaService'];

	function atividadeService(baseService, $q, escolaService) {

		var service = this;

		service.ListarAtividade = listarAtividade;
		service.salvar = salvar;
		service.removerFoto = removerFoto;

		return service;

		function listarAtividade(pagina) {
			var escola = escolaService.obterEscola();
			var def = $q.defer();
			baseService.call("GET", "atividade/" + pagina + "/" + escola.Id, "")
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		}

		function salvar(atividade){
			var escola = escolaService.obterEscola();
			var def = $q.defer();
			atividade.EscolaId = escola.Id;
			baseService.call("POST", "atividade/salvar", atividade)
				.then(function(data){
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		}

		function removerFoto(request) {
            var def = $q.defer();
            baseService.call("POST", "atividade/remover-foto", request)
                .then(function (data) {
                    def.resolve(data);
                }, function (error) {
                    def.reject(error);
                });
            return def.promise;
        };
	}
})();