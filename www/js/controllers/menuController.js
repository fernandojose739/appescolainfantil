﻿(function() {
	'use strict';

	//Assinatura da controller
	app.controller('menuController', menuController);

	//Injeção das dependências
	menuController.$inject = [
		'usuarioService'
	];

	//Construtor
	function menuController(usuarioService) {

		var ctrl = this;

		setEvents();
		setProperties();
		initialize();

		return ctrl;
        
        function setEvents() {	
            		
		}

		function setProperties() {
			ctrl.usuario = usuarioService.obterDoLocalStorage('usuario');
		}

		function initialize() {
		}
	}
})();