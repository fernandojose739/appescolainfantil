﻿(function() {
	'use strict';

	//Assinatura da controller
	app.controller('comunicadoController', Comunicados);

	//Injeção das dependências
	Comunicados.$inject = [
        'localStorageService',
		'comunicadoService',
		'usuarioService'
	];

	//Construtor
	function Comunicados(localStorageService, comunicadoService, usuarioService) {

		var ctrl = this;

		setEvents();
		setProperties();
		initialize();

		return ctrl;
        
        function setEvents() {	
			ctrl.confirmar = confirmar;
			ctrl.setRelatorioExibir = setRelatorioExibir;
		}

		function setProperties() {
			ctrl.relatorioExibir = false;
			ctrl.carregando = false;
			ctrl.confirmacaoRealizada = false;
			ctrl.relatorio = [];
			ctrl.usuario = usuarioService.obterDoLocalStorage('usuario');
			ctrl.comunicado = JSON.parse(localStorageService.obter('comunicado'));
		};

		function initialize() {
			validarSeUsuarioJaConfirmou();
			marcarComoLido();
			listarRelatorio();
		};

		function setRelatorioExibir(valor){
			ctrl.relatorioExibir = valor;
		};

		function validarSeUsuarioJaConfirmou(){
			if (!ctrl.comunicado.Confirmar) return;

			for(var x = 0; x < ctrl.comunicado.ComunicadoConfirmarUsuarios.length; x++){
				if (ctrl.comunicado.ComunicadoConfirmarUsuarios[x].UsuarioId == ctrl.usuario.UsuarioId){
					ctrl.confirmacaoRealizada = true;
					return;
				}
			}
		};

		function listarRelatorio(){
			if (ctrl.usuario.UsuarioId != 1) return;
			
			comunicadoService.listarRelatorio(ctrl.comunicado.Id).then(function(data) {
				ctrl.relatorio = data.data;
			}, function(data) {
			});
		};

		function marcarComoLido(){
			comunicadoService.marcarComoLido(obterConfirmarRequest()).then(function(data) {
			}, function(data) {
			});			
		};

		function confirmar(){
			ctrl.carregando = true;
			comunicadoService.confirmar(obterConfirmarRequest()).then(function(data) {
				ctrl.carregando = false;
				ctrl.confirmacaoRealizada = true;
			}, function(data) {
				ctrl.carregando = false;
			});			
		};

		function obterConfirmarRequest(){
			var request = {
				UsuarioId: ctrl.usuario.UsuarioId,
				ComunicadoId: ctrl.comunicado.Id
			};

			return request;
		};
	}
})();