﻿(function() {
	'use strict';

	//Assinatura da controller
	app.controller('painelController', painelController);

	//Injeção das dependências
	painelController.$inject = [
        '$rootScope',
		'$scope',
		'usuarioService',
		'$timeout'
	];

	//Construtor
	function painelController($rootScope, $scope, usuarioService, $timeout) {

		var ctrl = this;

		setEvents();
		setProperties();
		initialize();

		return ctrl;
        
        function setEvents() {
		}

		function setProperties() {
		}

		function initialize() {
			usuarioService.atualizaPushNotificationAndroidRegistrationId();
			usuarioService.listar();
		}
	}
})();