﻿
(function() {
	'use strict';

	//Assinatura da controller
	app.controller('medicamentosController', medicamentosController);

	//Injeção das dependências
	medicamentosController.$inject = [
        'medicamentoService',
        'localStorageService',
        '$location',
        '$scope',
		'escolaService'
	];

	//Construtor
	function medicamentosController(medicamentoService, localStorageService, $location, $scope, escolaService) {

		var ctrl = this;

		setEvents();
		setProperties();
		attachEvents();
		initialize();

		return ctrl;
        
        function setEvents() {
            ctrl.editar = editar;	
			ctrl.novo = novo;	
		}

		function setProperties() {
            ctrl.medicamentos = [];
			ctrl.escola = escolaService.obterEscola();
		}

		function attachEvents() {
		}

		function initialize() {
           
		}
        
        function _listar() {
			medicamentosService.listar().then(function(data) {
				ctrl.medicamentos = data.data;
			}, function(data) {
			});
		}
        
        function editar(medicamentos){
            localStorageService.salvar('medicamento', medicamento);
            $location.path('/app/medicamento')
        }
        
		function novo(){
			var medicamento = {
				Remedio: '',
				Dose: '',
				Horario: '',
				De: '',
				Ate: '',
				AlunoId: ''
			};

			localStorageService.salvar('medicamento', medicamento);
            $location.path('/app/medicamento')
		};
    }
})();