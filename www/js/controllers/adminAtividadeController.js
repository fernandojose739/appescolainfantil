﻿(function () {
	'use strict';

	//Assinatura da controller
	app.controller('adminAtividadeController', adminAtividadeController);

	//Injeção das dependências
	adminAtividadeController.$inject = [
        'localStorageService',
		'grupoService',
		'atividadeService',
		'$location'
	];

	//Construtor
	function adminAtividadeController(localStorageService, grupoService, atividadeService, $location) {

		var ctrl = this;

		setEvents();
		setProperties();
		attachEvents();
		initialize();

		return ctrl;

        function setEvents() {
			ctrl.salvar = salvar;
		}

		function setProperties() {
			ctrl.carregando = false;
			ctrl.atividade = JSON.parse(localStorageService.obter('atividade'));
			ctrl.atividade.GrupoId = 0;
			if (ctrl.atividade.GrupoViewModel != undefined) {
				ctrl.atividade.GrupoId = ctrl.atividade.GrupoViewModel.Id;
			}
			ctrl.grupos = [];
		}

		function attachEvents() {
		}

		function initialize() {
			carregarGrupos();
		}

		function carregarGrupos() {
			grupoService.listar()
				.then(function (data) {
					ctrl.grupos = data.data;
				});
		}

		function salvar() {
			ctrl.carregando = true;
			atividadeService.salvar(ctrl.atividade)
				.then(function (data) {
					localStorageService.salvar('atividade', data.data);
					$location.path('/app/adminAtividadeImagens');
					ctrl.carregando = false;
				}, function (error) {
					alert("Erro ao salvar atividade");
					ctrl.carregando = false;
				});
		}
	}
})();