﻿(function() {
	'use strict';

	//Assinatura da controller
	app.controller('videosController', Videos);

	//Injeção das dependências
	Videos.$inject = [
        'videoService',
        'localStorageService',
        '$location',
        '$scope',
        '$sce'
	];

	//Construtor
	function Videos(videoService, localStorageService, $location, $scope, $sce) {

		var ctrl = this;

		setEvents();
		setProperties();
		attachEvents();
		initialize();

		return ctrl;
        
        function setEvents() {
            ctrl.LoadMore = loadMore;
            ctrl.GetIframeSrc = getIframeSrc;		
		}

		function setProperties() {
            ctrl.noMoreItemsAvailable = false;
            ctrl.pagina = -1;
            ctrl.videos = [];
		}

		function attachEvents() {
		}

		function initialize() {
           
		}
        
        function _listarVideo() {
			videoService.ListarTodos(ctrl.pagina).then(function(data) {
				ctrl.videos = ctrl.videos.concat(data.data);
                
                if (data.data.length <= 0){
                    ctrl.noMoreItemsAvailable = true;
                }
                
                $scope.$broadcast('scroll.infiniteScrollComplete');
			}, function(data) {
				ctrl.noMoreItemsAvailable = true;
                $scope.$broadcast('scroll.infiniteScrollComplete');
			});
		}
        
        function getIframeSrc(videoId) {
                var src = $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + videoId);
                return src;
        };
        
        function loadMore() {
            ctrl.pagina = ctrl.pagina + 1;
             _listarVideo();
        };
    }
})();