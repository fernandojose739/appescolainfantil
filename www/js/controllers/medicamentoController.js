﻿(function () {
	'use strict';

	//Assinatura da controller
	app.controller('medicamentoController', medicamentoController);

	//Injeção das dependências
	medicamentoController.$inject = [
        'localStorageService',
		'alunoService',
		'medicamentoService',
		'$location'
	];

	function medicamentoController(localStorageService, alunoService, medicamentoService, $location) {

		var ctrl = this;

		setEvents();
		setProperties();
		attachEvents();
		initialize();

		return ctrl;

        function setEvents() {
			ctrl.salvar = salvar;
		}

		function setProperties() {
			ctrl.carregando = false;
			ctrl.medicamento = JSON.parse(localStorageService.obter('medicamento'));
			ctrl.alunos = [];
		}

		function attachEvents() {
		}

		function initialize() {
			listarAlunos();
		}

		function listarAlunos() {
			alunoService.listar()
				.then(function (data) {
					ctrl.alunos = data.data;
				});
		}

		function salvar() {
			ctrl.carregando = true;
			medicamentoService.salvar(ctrl.medicamento)
				.then(function (data) {
					$location.path('/app/medicamentos');
					ctrl.carregando = false;
				}, function (error) {
					alert("Erro ao salvar medicamento");
					ctrl.carregando = false;
				});
		}
	}
})();