﻿(function() {
	'use strict';

	//Assinatura da controller
	app.controller('comunicadosController', Comunicados);

	//Injeção das dependências
	Comunicados.$inject = [
        'comunicadoService',
        'localStorageService',
        '$location',
        '$scope'
	];

	//Construtor
	function Comunicados(comunicadoService, localStorageService, $location, $scope) {

		var ctrl = this;

		setEvents();
		setProperties();
		attachEvents();
		initialize();

		return ctrl;
        
        function setEvents() {
            ctrl.VerMais = verMais;	
            ctrl.LoadMore = loadMore;		
		}

		function setProperties() {
            ctrl.noMoreItemsAvailable = false;
            ctrl.pagina = -1;
            ctrl.comunicados = [];
		}

		function attachEvents() {
		}

		function initialize() {
		}
        
        function _listarTodos() {
			comunicadoService.ListarTodos(ctrl.pagina).then(function(data) {
				ctrl.comunicados = ctrl.comunicados.concat(data.data);
                
                if (data.data.length <= 0){
                    ctrl.noMoreItemsAvailable = true;
                }
                
                $scope.$broadcast('scroll.infiniteScrollComplete');
			}, function(data) {
				ctrl.noMoreItemsAvailable = true;
                $scope.$broadcast('scroll.infiniteScrollComplete');
			});
		}
        
        function verMais(comunicado){
            localStorageService.salvar('comunicado', comunicado);
            $location.path('/app/comunicado')
        }
        
        function loadMore() {
            ctrl.pagina = ctrl.pagina + 1;
             _listarTodos();
        };
    }
})();