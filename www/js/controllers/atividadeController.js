﻿(function() {
	'use strict';

	//Assinatura da controller
	app.controller('atividadeController', Atividades);

	//Injeção das dependências
	Atividades.$inject = [
        'localStorageService',
		'escolaService'
	];

	//Construtor
	function Atividades(localStorageService, escolaService) {

		var ctrl = this;

		setEvents();
		setProperties();
		attachEvents();
		initialize();

		return ctrl;
        
        function setEvents() {	
            		
		}

		function setProperties() {
			ctrl.atividade = JSON.parse(localStorageService.obter('atividade'));
			ctrl.escola = escolaService.obterEscola();
		}

		function attachEvents() {
		}

		function initialize() {
		}
	}
})();