﻿(function() {
	'use strict';

	//Assinatura da controller
	app.controller('sugestaoController', Sugestao);

	//Injeção das dependências
	Sugestao.$inject = [
        'sugestaoService',
        'localStorageService',
        '$location',
        '$scope'
	];

	//Construtor
	function Sugestao(sugestaoService, localStorageService, $location, $scope) {

		var ctrl = this;

		setEvents();
		setProperties();
		attachEvents();
		initialize();

		return ctrl;
        
        function setEvents() {
            ctrl.Novo = novo;
            ctrl.Cancelar = cancelar;
            ctrl.Adicionar = adicionar;
            ctrl.AdicionarEmail = adicionarEmail;
            ctrl.Editar = editar;	
            ctrl.LoadMore = loadMore;
            ctrl.editarDisabled = false;
		};
        
        function cancelar(){
            ctrl.showAdicionar = false;
        };

		function setProperties() {
            ctrl.noMoreItemsAvailable = false;
            ctrl.pagina = -1;
            ctrl.email = localStorageService.obter("usuarioEmail");
            ctrl.sugestoes = [];
            ctrl.showAdicionar = false;
            ctrl.showEmail = false;
            ctrl.erro = "";
            ctrl.erroListarTodos = "";
            ctrl.form = {};
            ctrl.showSemSugestao = false;
		};

		function attachEvents() {
		};

		function initialize() {
            // loadMore();
		};
        
        function _validarEmailEListarTodos(){
            if (ctrl.email === "undefined" || ctrl.email === undefined || ctrl.email === "") {
                ctrl.showEmail = true;
                return;
            }
            
            _listarTodos();
        }
        
        function _listarTodos() {
            var request = {
                Pagina: ctrl.pagina,
                Email: ctrl.email
            };
            
			sugestaoService.ListarTodos(request).then(
                function(data) {
                    ctrl.erroListarTodos = "";
                    ctrl.sugestoes = ctrl.sugestoes.concat(data.data);
                    
                    if (data.data.length <= 0){
                        ctrl.noMoreItemsAvailable = true;
                    }

                    ctrl.showSemSugestao = false;                    
                    if (ctrl.sugestoes.length <= 0){
                        ctrl.showSemSugestao = true;
                    }
                    
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                },
                function(data) {
                    ctrl.erroListarTodos = "Favor verifique a sua internet e tente novamente!";
                    console.log("teste error");
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                }
            );
		};
        
        function adicionar(){
            sugestaoService.Adicionar(ctrl.form).then(
                function(dataSucesso) {
                    ctrl.sugestoes = ctrl.sugestoes.concat(dataSucesso.data);
                    ctrl.showAdicionar = false;
                    ctrl.showSemSugestao = false;
                },
                function(dataError) {
                    ctrl.erro = "erro ao inserir sugestão, favor tentar novamente mais tarde!";
                }
            )
        };
        
        function editar(sugestao){
            ctrl.form = sugestao;
            ctrl.form.Email = ctrl.email;
            ctrl.showAdicionar = true;
            ctrl.editarDisabled = true;
        };
        
        function loadMore() {
            
            if(ctrl.sugestoes.length > 0)
                ctrl.pagina = ctrl.pagina + 1;
            else
                ctrl.pagina = 0;
                
             _validarEmailEListarTodos();
        };
        
        function adicionarEmail(){
            localStorageService.salvarString("usuarioEmail", ctrl.email);
            ctrl.showEmail = false;
        };
        
        function novo(){
            ctrl.showAdicionar = true;
            ctrl.form = {};
            ctrl.form.Email = ctrl.email;
            ctrl.editarDisabled = false;
        };
    }
})();