﻿(function () {
	'use strict';

	//Assinatura da controller
	app.controller('chatConversaController', chatConversaController);

	//Injeção das dependências
	chatConversaController.$inject = [
        'localStorageService',
		'chatService',
        '$interval',
		'$ionicHistory',
		'$ionicScrollDelegate',
		'usuarioService',
		'notificacaoService'
	];

	//Construtor
	function chatConversaController(localStorageService, chatService, $interval, $ionicHistory, $ionicScrollDelegate, usuarioService, notificacaoService) {

		var ctrl = this;

		setEvents();
		setProperties();
		initialize();

		return ctrl;

        function setEvents() {
			ctrl.responder = responder;
			ctrl.voltar = voltar;
		};

		function setProperties() {
			ctrl.ultimoId = 0;
			ctrl.pararTimer;
			ctrl.carregando = false;
			ctrl.usuario = usuarioService.obterDoLocalStorage('usuario');
            ctrl.chat = JSON.parse(localStorageService.obter('chat'));

			ctrl.form = {
				De: ctrl.usuario.UsuarioId,
				Mensagem: '',
				ChatId: ctrl.chat.Id,
			};
		};

		function initialize() {
			atualizarChat();
		};

		function responder() {
			ctrl.carregando = true;
			chatService.responder(ctrl.form).then(function (data) {
				listarMensagens();
				ctrl.carregando = false;

				var chat = JSON.parse(localStorageService.obter('chat'));
				if (chat.GrupoPara == null || chat.GrupoPara == undefined) {
					var registrationIds = [];

					var de = usuarioService.obterResgistraionIdPorUsuario(chat.De);
					if (de !== null && de !== undefined) {
						registrationIds.push(usuarioService.obterResgistraionIdPorUsuario(chat.De));
					}

					var para = usuarioService.obterResgistraionIdPorUsuario(chat.Para);
					if (para !== null && para !== undefined) {
						registrationIds.push(usuarioService.obterResgistraionIdPorUsuario(chat.Para));
					}
				}

				var request = {
					registration_ids: registrationIds,
					data: {
						title: obterMeuNome(ctrl.form.De),
						message: ctrl.form.Mensagem,
						image: "https://dl.dropboxusercontent.com/u/887989/antshot.png"
					}
				};
				notificacaoService.enviar(request);

				ctrl.form.Mensagem = '';
			}, function (data) {
				ctrl.carregando = false;
			});
		};

		function obterMeuNome(usuarioId) {
			for (var x = 0; x < ctrl.chat.ChatMensagens.length; x++) {
				if (usuarioId == ctrl.chat.ChatMensagens[x].De) {
					return ctrl.chat.ChatMensagens[x].UsuarioDe.Nome;
				}
			}
		}

        function atualizarChat() {
            if (angular.isDefined(ctrl.pararTimer)) return;
            ctrl.pararTimer = $interval(function () {
                listarMensagens();
            }, 20000);
        };

        function pararDeAtualizar() {
            if (angular.isDefined(ctrl.pararTimer)) {
                $interval.cancel(ctrl.pararTimer);
                ctrl.pararTimer = undefined;
            }
        };

		function voltar() {
			pararDeAtualizar();
			$ionicHistory.goBack();
		};

		function obterUltimoId() {
			ctrl.ultimoId = 0;
			for (var x = 0; x < ctrl.chat.ChatMensagens.length; x++) {
				ctrl.ultimoId = ctrl.chat.ChatMensagens[x].Id;
			}
		};

		function listarMensagens() {
			obterUltimoId();
			chatService.listarMensagens(ctrl.chat.Id, ctrl.ultimoId).then(function (data) {
				ctrl.chat.ChatMensagens = ctrl.chat.ChatMensagens.concat(data.data);

				if (data.data.length > 0) {
					$ionicScrollDelegate.scrollBottom(true);
				}
			}, function (data) {
			});
		};

	}
})();