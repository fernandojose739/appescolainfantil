﻿(function() {
	'use strict';

	//Assinatura da controller
	app.controller('adminAtividadesController', adminAtividadesController);

	//Injeção das dependências
	adminAtividadesController.$inject = [
        'atividadeService',
        'localStorageService',
        '$location',
        '$scope',
		'escolaService'
	];

	//Construtor
	function adminAtividadesController(atividadeService, localStorageService, $location, $scope, escolaService) {

		var ctrl = this;

		setEvents();
		setProperties();
		attachEvents();
		initialize();

		return ctrl;
        
        function setEvents() {
            ctrl.editar = editar;	
            ctrl.LoadMore = loadMore;	
			ctrl.novo = novo;	
		}

		function setProperties() {
            ctrl.noMoreItemsAvailable = false;
            ctrl.pagina = -1;
            ctrl.atividades = [];
			ctrl.escola = escolaService.obterEscola();
		}

		function attachEvents() {
		}

		function initialize() {
           
		}
        
        function _listarAtividade() {
			atividadeService.ListarAtividade(ctrl.pagina).then(function(data) {
				ctrl.atividades = ctrl.atividades.concat(data.data);
                
                if (data.data.length <= 0){
                    ctrl.noMoreItemsAvailable = true;
                }
                
                $scope.$broadcast('scroll.infiniteScrollComplete');
			}, function(data) {
				ctrl.noMoreItemsAvailable = true;
                $scope.$broadcast('scroll.infiniteScrollComplete');
			});
		}
        
        function editar(atividade){
            localStorageService.salvar('atividade', atividade);
            $location.path('/app/adminAtividade')
        }
        
        function loadMore() {
            ctrl.pagina = ctrl.pagina + 1;
             _listarAtividade();
        };

		function novo(){
			var atividade = {
				Titulo: '',
				CompletaApp: '',
			};

			localStorageService.salvar('atividade', atividade);
            $location.path('/app/adminAtividade')
		};
    }
})();