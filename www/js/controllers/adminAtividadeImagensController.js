﻿(function () {
	'use strict';

	//Assinatura da controller
	app.controller('adminAtividadeImagensController', adminAtividadeImagensController);

	//Injeção das dependências
	adminAtividadeImagensController.$inject = [
        'localStorageService',
		'atividadeService',
		'$location',
        '$cordovaCamera',
        '$ionicLoading'
	];

	//Construtor
	function adminAtividadeImagensController(localStorageService, atividadeService, $location, $cordovaCamera, $ionicLoading) {

		var ctrl = this;

		setEvents();
		setProperties();
		initialize();

		return ctrl;

        function setEvents() {
			ctrl.selecionar = selecionar;
            ctrl.remover = remover;
		}

		function setProperties() {
			ctrl.atividade = JSON.parse(localStorageService.obter('atividade'));
		}

		function initialize() {
		}

		function remover(arquivo) {
            var atividadeViewModel = {
                Id: ctrl.atividade.Id,
                Arquivo: arquivo
            };

            atividadeService.removerFoto(atividadeViewModel)
                .then(function (data) {
                    ctrl.atividade.Fotos = data.data;
                }, function (error) {
				});
        };

		function selecionar() {
            var options = {
                quality: 25,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                encodingType: Camera.EncodingType.JPEG,
                correctOrientation: true
            };

            $cordovaCamera.getPicture(options).then(
                function (imageURI) {
                    adicionar(imageURI);
                },
                function (err) {
                    $ionicLoading.show({ template: 'Erro de carregamento...', duration: 3000 })
                }
            );
        };

        function adicionar(imageURI) {

            var params = {};
            params.AtividadeId = ctrl.atividade.Id;

            var options = new FileUploadOptions();
            options.fileKey = "recFile";
            options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
            options.mimeType = "image/jpeg";
            options.chunkedMode = false;
            options.httpMethod = "POST";
            options.headers = { 'Token': '5b5fd8ba4bb6d9e12bf93d5373409ee7' };
            options.params = params;

            var uri = encodeURI("http://www.ecoaprender.com.br/api/atividade/upload")
            var ft = new FileTransfer();
            ft.upload(imageURI, uri, onSuccess, onFail, options);
            ft.onprogress = onProgress;
        }

        function onProgress(progressEvent) {
            if (progressEvent.lengthComputable) {
                var uploadProgress = (progressEvent.loaded / progressEvent.total) * 100;
                $ionicLoading.show({
                    template: "Enviando: " + Math.floor(uploadProgress) + "%"
                });
                if (uploadProgress > 99) {

                }
            } else {
                $ionicLoading.hide();
            }
        };

        function onFail(message) {
            if (message.indexOf('cancelled') < 0) {
                alert('Error' + message);
            }
        }

        function onSuccess(msg) {
            var response = JSON.parse(msg.response);
            if (msg.responseCode == "200") {

                $ionicLoading.show({
                    template: "Foto enviada com sucesso!",
                    duration: 1200
                });
                
                ctrl.atividade.Fotos.push(response);
            } else {
                $ionicLoading.show({
                    template: "Erro de upload",
                    duration: 1000
                });
            }
        }
	}
})();