﻿(function () {
	'use strict';

	//Assinatura da controller
	app.controller('chatNovoController', chatNovoController);

	//Injeção das dependências
	chatNovoController.$inject = [
        'chatService',
        'localStorageService',
        '$location',
        '$scope',
        '$sce',
		'paraTipoService',
		'usuarioService',
		'grupoService',
		'notificacaoService'
	];

	//Construtor
	function chatNovoController(chatService, localStorageService, $location, $scope, $sce, paraTipoService, usuarioService, grupoService, notificacaoService) {

		var ctrl = this;

		setEvents();
		setProperties();
		initialize();

		return ctrl;

        function setEvents() {
			ctrl.exibirParaPorParaTipoId = exibirParaPorParaTipoId;
			ctrl.enviar = enviar;
		};

		function setProperties() {
			ctrl.usuario = usuarioService.obterDoLocalStorage('usuario');
            ctrl.noMoreItemsAvailable = false;
            ctrl.pagina = -1;
            ctrl.Chat = [];
			ctrl.tipos = [];
			ctrl.usuarios = [];
			ctrl.grupos = [];
			ctrl.exibirListaDeUsuarios = true;
			ctrl.carregando = false;
			ctrl.form = {
				De: ctrl.usuario.UsuarioId,
				ParaTipo: '',
				Para: '',
				GrupoPara: '',
				UsuarioParaId: '',
				Assunto: '',
				Mensagem: ''
			};
		};

		function initialize() {
           	listarParaTipo();
			listarUsuarios();
			listarGrupos();
		};

        function listarParaTipo() {
			paraTipoService.listar().then(function (data) {
				ctrl.tipos = data.data;
			}, function (data) {
			});
		};

		function listarUsuarios() {
			usuarioService.listar().then(function (data) {
				ctrl.usuarios = data.data;
			}, function (data) {
			});
		};

		function listarGrupos() {
			grupoService.listar().then(function (data) {
				ctrl.grupos = data.data;
			}, function (data) {
			});
		};

		function exibirParaPorParaTipoId() {
			if (ctrl.ParaTipo == 'usuario') {
				ctrl.exibirListaDeUsuarios = true;
			} else {
				ctrl.exibirListaDeUsuarios = false;
			}
		};

		function enviar() {
			ctrl.carregando = true;
			chatService.enviar(ctrl.form).then(function (data) {
				ctrl.carregando = false;

				if (ctrl.form.ParaTipo == 'usuario') {
					var registrationId = usuarioService.obterResgistraionIdPorUsuario(ctrl.form.Para);
				}

				var request = {
					registration_ids: [registrationId],
					data: {
						title: ctrl.form.Assunto,
						message: ctrl.form.Mensagem,
						image: "https://dl.dropboxusercontent.com/u/887989/antshot.png"
					}
				};

				notificacaoService.enviar(request);
				$location.url('/app/chats');
			}, function (data) {
				ctrl.carregando = false;
			});
		};		
    }
})();