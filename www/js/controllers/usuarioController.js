﻿(function() {
	'use strict';

	//Assinatura da controller
	app.controller('usuarioController', usuarioController);

	//Injeção das dependências
	usuarioController.$inject = [
        'usuarioService',
        'localStorageService',
        '$location',
        '$scope',
        '$sce',
		'$rootScope'
	];

	//Construtor
	function usuarioController(usuarioService, localStorageService, $location, $scope, $sce, $rootScope) {

		var ctrl = this;

		setEvents();
		setProperties();
		initialize();

		return ctrl;
        
        function setEvents() {
            ctrl.login = login;	
			ctrl.carregando = false;
		};

		function setProperties() {
            ctrl.noMoreItemsAvailable = false;
            ctrl.pagina = -1;
            ctrl.usuarioController = [];
		};
		
		function initialize() {
			validarSeJaEstaLogado();
		};

		function validarSeJaEstaLogado(){
			var usuario = usuarioService.obterDoLocalStorage('usuario');
			if(usuario != undefined && usuario != null){
				$rootScope.escolaNome = usuario.Escola.Nome;
				$location.path('/app/painel');
			}
		}
        
        function login() {
			ctrl.carregando = true;
			ctrl.alerta = '';
			usuarioService.login(ctrl.email, ctrl.senha).then(function(data) {
				ctrl.usuarioController = ctrl.usuarioController.concat(data.data);
				localStorageService.salvar("usuario", data.data);
				$rootScope.escolaNome = data.data.Escola.Nome;
				$location.url('/app/painel');
			}, function(data) {
				ctrl.alerta = 'usuário ou senha inválido';
				ctrl.carregando = false;
			});
		};
    }
})();